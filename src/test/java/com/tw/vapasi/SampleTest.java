package com.tw.vapasi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SampleTest {
    @Test
    void sample_test_1() {
        Sample sample = new Sample(1);
        Assertions.assertEquals(1, sample.getId());
    }

    @Test
    void sample_test_2() {
        Sample sample = new Sample(1);
        sample.setId(2);
        Assertions.assertEquals(2, sample.getId());
    }
}
