package com.tw.vapasi;

public class Sample {
    private int id;

    public Sample(int id) {

        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
